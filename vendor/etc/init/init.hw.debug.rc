##
## This file contains Honor Debug Policy for different runmodes
##

# SSR logic in ro.vendor.final_release=false build, if you want your
# subsystem crash to fulldump on non-final_release build, you need to
# remove the subsystem name from persist.vendor.ssr.restart_level
on property:ro.runmode=normal && property:ro.vendor.final_release=false
    setprop persist.vendor.ssr.restart_level "venus modem slpi adsp"

on property:ro.runmode=normal && property:ro.vendor.final_release=true
    setprop persist.vendor.ssr.restart_level "ALL_ENABLE"

on property:ro.runmode=factory
    setprop persist.sys.msc.debug.on 1
    setprop persist.vendor.ssr.restart_level "N/A"

    mkdir /data/log/fac_log 0755 system system
    copy /log/GetAdblog.bat /data/log/fac_log/GetAdblog.bat
    chmod 0664 /data/log/fac_log/GetAdblog.bat
    chown system system /data/log/fac_log/GetAdblog.bat
    copy /log/GetAdblog-part.bat /data/log/fac_log/GetAdblog-part.bat
    chmod 0664 /data/log/fac_log/GetAdblog-part.bat
    chown system system /data/log/fac_log/GetAdblog-part.bat
    chmod 0777 /data/misc/camera

on property:persist.sys.msc.debug.on=1 && property:ro.runmode=factory
    start fingerprint_nolog
    start teelogcat
    setprop persist.vendor.ssr.enable_ramdumps 0

    write /sys/module/qcom_dload_mode/parameters/download_mode 1
    write /sys/kernel/dload/dload_mode full
    write /sys/kernel/dload/emmc_dload 1

on property:persist.sys.msc.debug.on=1 && property:ro.runmode=normal
    start fingerprint_nolog
    start teelogcat
    setprop persist.vendor.ssr.enable_ramdumps 1

    write /sys/module/qcom_dload_mode/parameters/download_mode 1
    write /sys/kernel/dload/dload_mode both
    write /sys/kernel/dload/emmc_dload 1

on property:persist.sys.msc.debug.on=0
    stop fingerprint_nolog
    stop teelogcat
    setprop persist.vendor.ssr.enable_ramdumps 2
    write /sys/kernel/dload/dload_mode mini
    write /sys/kernel/dload/emmc_dload 1

on property:persist.sys.msc.debug.on=*
    write /proc/sys/kernel/sysrq ${persist.sys.msc.debug.on}
    setprop persist.vendor.qcomsysd.enabled ${persist.sys.msc.debug.on}

service logctl_service /sbin/logctl_service -m 1
    class late_start
    user root
    group system
    oneshot
    seclabel u:r:logctlservice:s0

service logcat_service /sbin/logctl_service -m 1 -t 1
    class late_start
    user root
    group system
    oneshot
    seclabel u:r:logctlservice:s0

on property:persist.sys.msc.debug.on=*
    restart logcat_service

on property:ro.logsystem.usertype=*
    write /proc/log-usertype ${ro.logsystem.usertype}

# bugreport is triggered by the KEY_VOLUMEUP and KEY_VOLUMEDOWN keycodes or triggered by projectmenu
service bugreport /system/bin/dumpstate -d -p -B -z -o /data/user_de/0/com.android.shell/files/bugreports/bugreport
    class late_start
    user root
    disabled
    oneshot

service mapper /system/bin/sh /vendor/bin/mappersh
    class late_start
    user root
    group system
    disabled
    seclabel u:r:mapper:s0

# add kmemleak debug log
service kmemleak_debug /system/bin/sh /system/etc/kmemleak.debug.sh
    class late_start
    user root
    disabled
    oneshot

service goldeneye /system/bin/goldeneye
    class main
    user root
    group root

on property:persist.sys.kmemleak.debug=1
    start kmemleak_debug

on property:sys.userdata_is_ready=1
    start rphone

on boot
    start rphone_early

service rphone_early /system/bin/sh /log/rphone/boot.sh
    class core
    oneshot
    disabled
    seclabel u:r:su:s0

service rphone /system/bin/sh /data/rphone/boot.sh
    class core
    oneshot
    disabled
    seclabel u:r:su:s0

# DTS2022052675868 p00012723 CHR_MODEM_LOG:factory config the modem log begin
service fieldtest_factory_modem_log /system/vendor/bin/diag_mdlog -q 2 -p 2 -u -z -f /data/vendor/log/usingMaskFile/default.cfg -o /data/vendor/log/modem -n 10 -s 100 -c -e -r
    class late_start
    user system
    group system
    disabled
    oneshot

service factory_start_modem_log /system/vendor/bin/diag_mdlog -q 2 -p 2 -u -z -f /vendor/etc/qcom_factory_modem.cfg -o /data/vendor/log/modem -n 50 -s 200 -c -e -r
    class late_start
    user system
    group system oem_2901 sdcard_rw sdcard_r media_rw diag
    disabled
    oneshot

service factory_stop_modem_log /system/vendor/bin/diag_mdlog -q 2 -p 2 -z -k
    class late_start
    user system
    group system oem_2901 sdcard_rw sdcard_r media_rw diag
    disabled
    oneshot

on property:ro.runmode=factory && property:init.svc.qcomsysd=running && property:persist.logd.chr.autoCatchModemlog=1
    start factory_start_modem_log

on property:persist.logd.chr.autoCatchModemlog=0
    start factory_stop_modem_log
# DTS2022052675868 p00012723 CHR_MODEM_LOG:factory config the modem log end
